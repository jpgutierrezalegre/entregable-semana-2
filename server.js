require("dotenv").config();
const express = require('express');
const body_parser = require('body-parser');
const app = express();

let port = process.env.PORT || 3000;
const URL_BASE = "/techu/v1/";
let usersFile = require('./user.json');

app.listen(port, function() {
  console.log('Node JS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

// Operación GET all
app.get(URL_BASE + 'users',
    function(request, response) {
      response.send(usersFile);
});

// Operación GET by id
app.get(URL_BASE + 'users/:id',
    function(request, response) {
      const user = usersFile.find(x=>x.id==request.params.id);
      let status = 200;
      if (user == undefined) {
        status = 404;
      }
      response.status(status).send(user);
});

app.post(URL_BASE + 'users',
    function(request, response) {
      let newId = usersFile.reduce((prev, current) => (prev.id > current.id) ? prev : current).id + 1;
      let newUser = {
        "id": newId,
        "first_name": request.body.first_name,
        "last_name": request.body.last_name,
        "email": request.body.email,
        "password": request.body.password
      };
      usersFile.push(newUser);
      response.status(201).send({"msg": "Creado usuario con id: " + newUser.id });
});

app.put(URL_BASE + 'users/:id',
    function(request, response) {

      const id = request.params.id;
      let currentUserIndex = usersFile.findIndex(x=>{return x.id == id})

      if (currentUserIndex == -1) {
        response.status(404).send({"msg": "No se encontró usuario con id: " + id });
      }

      let user = {
        "id": id,
        "first_name": request.body.first_name,
        "last_name": request.body.last_name,
        "email": request.body.email,
        "password": request.body.password
      };

      usersFile[currentUserIndex] = user;
      response.status(202).send({"msg": "Actualizado usuario con id: " + user.id });
});

app.delete(URL_BASE + 'users/:id',
    function(request, response) {
      let id = request.params.id;

      let currentLength = usersFile.length
      usersFile = usersFile.filter(x=>{return x.id != id});

      if (usersFile.length < currentLength) {
        response.status(202).send({"msg": "Eliminado usuario con id: " + id});
      } else {
        response.status(404).send({"msg": "No se encontró usuario con id: " + id});
      }

});

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {
    console.log("login");
    console.log(request.body.email);
    console.log(request.body.password);
    var user = request.body.email;
    var pass = request.body.password;
    for(us of usersFile) {
      if(us.email == user) {
        if(us.password == pass) {
          us.logged = true;
          writeUserDataToFile(usersFile);
          console.log("Login correcto!");
          response.send({"msg" : "Login correcto.", "idUsuario" : us.id});
          return;
        }
      }
    }
    console.log("Login incorrecto.");
    response.send({"msg" : "Login incorrecto."});
});

// LOGOUT - users.json
app.post(URL_BASE + 'logout',
  function(request, response) {
    var userId = request.body.id;
    for(us of usersFile) {
      if(us.id == userId) {
        if(us.logged) {
          delete us.logged; // borramos propiedad 'logged'
          writeUserDataToFile(usersFile);
          console.log("Logout correcto!");
          response.send({"msg" : "Logout correcto.", "idUsuario" : us.id});
          return;
        }
      }
    }
    console.log("Logout incorrecto.");
    response.send({"msg" : "Logout incorrecto."});

});


function writeUserDataToFile(data) {
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./user.json", jsonUserData, "utf8",
   function(err) { //función manejadora para gestionar errores de escritura
     if(err) {
       console.log(err);
     } else {
       console.log("Datos escritos en 'user.json'.");
     }
   }); }
