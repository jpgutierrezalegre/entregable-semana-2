require("dotenv").config();
const express = require('express');
const body_parser = require('body-parser');
const app = express();
const request_json = require('request-json');

const DB_NAME = "" || process.env.MLAB_DBNAME;
const MLAB_APIKEY = 'apiKey=' + process.env.MLAB_APIKEY;
const URL_MLAB = "https://api.mlab.com/api/1/databases/"+ DB_NAME +"/collections/";

let port = process.env.PORT || 3000;
const URL_BASE = "/techu/v2/";

app.listen(port, function() {
  console.log('Node JS escuchando en el puerto ' + port);
});

app.use(body_parser.json());

// Operación GET all
app.get(URL_BASE + 'users',
    function(request, response) {
      const http_client = request_json.createClient(URL_MLAB);
      let fieldFilter = 'f={"_id":0}&'
      http_client.get("user?" + fieldFilter + MLAB_APIKEY, function(err, res, body){
        let responseBody = {};
        if (err) {
          responseBody = {'msg': 'Error en la petición a mLab'};
          response.status(500);
        } else {
          responseBody = body;
          response.status(200);
        }
        response.send(responseBody);
      });
});

const findUserById = function(id, includeDbId) {
  return new Promise((resolve, reject) => {

    let fieldFilter = includeDbId ? '' : 'f={"_id":0}&';
    let queryString = 'q={"id":' + id +'}&';

    const http_client = request_json.createClient(URL_MLAB);

    http_client.get("user?" + fieldFilter + queryString + MLAB_APIKEY, function(err, res, body){
      let responseBody = {};
      if (err) {
        reject(err);
      } else {
        resolve(body);
      }
    });
  });
}

// Operación GET by id
app.get(URL_BASE + 'users/:id',
    function(request, response) {

      let id = request.params.id;

      findUserById(id, false)
        .then((body)=>{
          let responseBody = {};
          if (body.length > 0) {
            responseBody = body;
            response.status(200);
          } else {
            responseBody = {'msg': 'Usuario no encontrado'};
            response.status(404);
          }
          response.send(responseBody);
        })
        .catch((err)=>{
          response.status(500).send({'msg': 'Error en la petición a mLab'});
        });

});

app.post(URL_BASE + 'users',
    function(request, response) {
      let newIdPromise = new Promise((resolve, reject) => {

        const http_client = request_json.createClient(URL_MLAB);
        let fieldFilter = 'f={"id":1}&'
        let sortString = 's={"id":-1}&l=1&';
        http_client.get("user?" + fieldFilter + sortString + MLAB_APIKEY, function(err, res, body){
          if (err) {
            reject();
          } else if (body.length > 0) {
            resolve(body[0].id + 1);
          } else {
            resolve(1);
          }
        });

      });

      newIdPromise
        .then((newId)=>{
          let newUser = {
            "id": newId,
            "first_name": request.body.first_name,
            "last_name": request.body.last_name,
            "email": request.body.email,
            "password": request.body.password
          };

          const http_client = request_json.createClient(URL_MLAB);
          http_client.post("user?" + MLAB_APIKEY, newUser, function(err, res, body) {
            let responseBody = {};
            if (err) {
              responseBody = {'msg': 'Error en la petición a mLab'};
              response.status(500);
            } else {
              responseBody = {"msg": "Creado usuario con id: " + newUser.id };
              response.status(201);
            }
            response.send(responseBody);
          });

        })
        .catch(()=>{
          response.status(500).send({'msg': 'Error en la petición a mLab'});
        });

});

app.put(URL_BASE + 'users/:id',
    function(request, response) {

      let id = request.params.id;

      findUserById(id, true)
        .then((responseUser)=>{

          if (responseUser.length == 0) {
            response.status(404).send({"msg": "No se encontró usuario con id: " + id });
          } else {
            let currentUser = responseUser[0];
            let updatedUser = {
              "id": currentUser.id,
              "first_name": request.body.first_name,
              "last_name": request.body.last_name,
              "email": request.body.email,
              "password": request.body.password
            };

            const httpClient = request_json.createClient(URL_MLAB);
            const dbId = currentUser._id.$oid;
            httpClient.put('user/' + dbId + '?' + MLAB_APIKEY, updatedUser, function(err, res, body){
              if (err) {
                response.status(500).send({'msg': 'Error en la petición a mLab'});
              } else {
                response.status(202).send({"msg": "Actualizado usuario con id: " + updatedUser.id });
              }
            });

          }

        })
        .catch((err)=>{
          response.status(500).send({'msg': 'Error en la petición a mLab'});
        });
});

app.delete(URL_BASE + 'users/:id',
    function(request, response) {

      let id = request.params.id;

      findUserById(id, true)
        .then((responseUser)=> {
          if (responseUser.length == 0) {
            response.status(404).send({"msg": "No se encontró usuario con id: " + id });
          } else {
            const httpClient = request_json.createClient(URL_MLAB);
            const dbId = responseUser[0]._id.$oid;
            httpClient.delete('user/' + dbId + '?' + MLAB_APIKEY, function(err, res, body){
              if (err) {
                response.status(500).send({'msg': 'Error en la petición a mLab'});
              } else {
                response.status(202).send({"msg": "Eliminado usuario con id: " + id });
              }
            });
          }
        })
        .catch((err)=>{
          response.status(500).send({'msg': 'Error en la petición a mLab'});
        });

});

// LOGIN - users.json
app.post(URL_BASE + 'login',
  function(request, response) {

    const findUserByEmail = new Promise((resolve, reject) => {

      let queryString = 'q={"email":"' + request.body.email +'"}&';

      const http_client = request_json.createClient(URL_MLAB);

      http_client.get("user?" + queryString + MLAB_APIKEY, function(err, res, body){
        let responseBody = {};
        if (err) {
          reject(err);
        } else {
          resolve(body);
        }
      });
    });

    findUserByEmail
      .then((responseUser)=>{
          if (responseUser.length == 0 || responseUser[0].password != request.body.password) {
            response.send({"msg" : "Login incorrecto."});
          } else {
            let currentUser = responseUser[0];
            currentUser.logged = true;

            const httpClient = request_json.createClient(URL_MLAB);
            const dbId = currentUser._id.$oid;
            httpClient.put('user/' + dbId + '?' + MLAB_APIKEY, currentUser, function(err, res, body){
              if (err) {
                response.status(500).send({'msg': 'Error en la petición a mLab'});
              } else {
                response.send({"msg" : "Login correcto.", "idUsuario" : currentUser.id});
              }
            });

          }
      })
      .catch((err)=>{
        response.status(500).send({'msg': 'Error en la petición a mLab'});
      });

});

// LOGOUT - users.json
app.post(URL_BASE + 'logout',
  function(request, response) {

    var userId = request.body.id;

    findUserById(userId, true)
      .then((responseUser)=>{
        if (responseUser.length == 0 || responseUser[0].logged != true) {
          response.send({"msg" : "Logout incorrecto."});
        } else {
          let currentUser = responseUser[0];
          delete currentUser.logged;

          const httpClient = request_json.createClient(URL_MLAB);
          const dbId = currentUser._id.$oid;
          httpClient.put('user/' + dbId + '?' + MLAB_APIKEY, currentUser, function(err, res, body){
            if (err) {
              response.status(500).send({'msg': 'Error en la petición a mLab'});
            } else {
              response.send({"msg" : "Logout correcto.", "idUsuario" : currentUser.id});
            }
          });

        }
      })
      .catch((err)=>{
        response.status(500).send({'msg': 'Error en la petición a mLab'});
      });

});

app.get(URL_BASE + 'users/:id/accounts',
  function(request, response) {
    let id = request.params.id;

    let fieldFilter = 'f={"_id":0}&'
    let queryString = 'q={"user_id":' + id +'}&';

    const http_client = request_json.createClient(URL_MLAB);

    http_client.get("account?" + fieldFilter + queryString + MLAB_APIKEY, function(err, res, body){
      let responseBody = {};
      if (err) {
        responseBody = {'msg': 'Error en la petición a mLab'};
        response.status(500);
      } else {
        if (body.length > 0) {
          responseBody = body;
          response.status(200);
        } else {
          responseBody = responseBody = {'msg': 'Cuentas no encontradas'};
          response.status(404);
        }
      }
      response.send(responseBody);
    });
});
